//
//  AppDelegate.swift
//  JWHelperKit
//
//  Created by 10126121@qq.com on 05/22/2019.
//  Copyright (c) 2019 10126121@qq.com. All rights reserved.
//

import UIKit
import JWHelperKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
//        var domain = "http://linjw.com/?a=1&c=4"
//        var params = ["a": 2, "b": 3]
//        let toText = domain.jw.api(addParams: params)
//        print(toText)
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }

}
