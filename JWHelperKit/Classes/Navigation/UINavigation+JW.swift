//
//  UINavigation+JW.swift
// @project：JWHelperKit
// @author：linjw(10126121@qq.com)
// @time: 2022/12/20
// Copyright © 2018年 Linjw. All rights reserved.
//

import Foundation
import UIKit

/// 拓展导航条
public extension JWNamespaceWrapper where T: UINavigationController {
    
    /// 获取KeyWindow下的rootViewController
    static func rootViewController() -> UIViewController? {
        return UIApplication.shared.keyWindow?.rootViewController
    }
    
    /// present
    static func present(_ viewController: UIViewController, animated: Bool = true, completion: (() -> Swift.Void)? = nil) {
        DispatchQueue.main.async {
            guard let fromViewController = UINavigationController.jw.topMost else { return }
            fromViewController.present(viewController, animated: animated, completion: completion)
        }
    }
    
    /// dismiss
    static func dismiss(animated: Bool = true, completion: (() -> Swift.Void)? = nil) {
        DispatchQueue.main.async {
            guard let fromViewController = UINavigationController.jw.topMost else { return }
            fromViewController.view.endEditing(true)
            fromViewController.dismiss(animated: animated, completion: completion)
        }
    }
    
    /// showDetail
    static func showDetail(_ viewController: UIViewController, sender: Any?) {
        DispatchQueue.main.async {
            guard let fromViewController = UINavigationController.jw.topMost else { return }
            fromViewController.showDetailViewController(viewController, sender: sender)
        }
    }
    
}
