//
//  UIViewController+Navigation+JW+Completion.swift
// @project：JWHelperKit
// @author：linjw(10126121@qq.com)
// @time: 2022/12/20
// Copyright © 2018年 Linjw. All rights reserved.
//

import UIKit
//import UINavigationControllerWithCompletionBlock

/// 拓展导航条
public extension JWNamespaceWrapper where T: UINavigationController {
    
    static func autoDismiss(animated: Bool = true, completion: (() -> Void)? = nil) {
        
        DispatchQueue.main.async {
            guard let currentVC = UINavigationController.jw.topMost else { return }
            if let count = currentVC.navigationController?.children.count, count > 1 {
                self.pop(animated, completion: completion)
            } else {
                self.dismiss(animated: animated, completion: completion)
            }
        }
        
    }
    
    static func push(_ viewController: UIViewController, animated: Bool = true, completion: (() -> Void)? = nil) {
        
        let top = UINavigationController.jw.topMost
        guard let navigationController = top?.navigationController else { return }
        viewController.hidesBottomBarWhenPushed = true
        navigationController.jwPushViewController(viewController, animated: animated, completion: completion)
    }
    
    static func pop(_ animated: Bool = true, completion: (() -> Void)? = nil) {
        
        let top = UINavigationController.jw.topMost
        guard let navigationController = top?.navigationController else { return }
        navigationController.jwPopViewController(animated: animated, completion: completion)
    }
    
    static func popToRoot(_ animated: Bool = true, completion: (() -> Void)? = nil) {
        
        guard let navigationController = UINavigationController.jw.topMost?.navigationController else { return }
        navigationController.jwPopToRootViewController(animated: animated, completion: completion)
    }
    
    static func popToViewController(_ viewController: UIViewController, animated: Bool = true, completion: (() -> Void)? = nil) {
        
        let top = UINavigationController.jw.topMost
        guard let navigationController = top?.navigationController else { return }
        navigationController.jwPopToViewController(viewController, animated: animated, completion: completion)
    }
    
    static func setViewControllers(_ viewControllers: [UIViewController], animated: Bool = true, completion: (() -> Void)? = nil) {
        guard let navigationController = UINavigationController.jw.topMost?.navigationController else { return }
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        navigationController.setViewControllers(viewControllers, animated: animated)
        CATransaction.commit()
    }
    
    /// 移除当前控制器，并push
    static func repush(_ viewController: UIViewController, animated: Bool = true, completion: (() -> Void)? = nil) {
        guard let navigationController = UIViewController.jw.topMost?.navigationController else { return  }
        var temArray: [UIViewController] = navigationController.viewControllers.dropLast()
        temArray.append(viewController)
        setViewControllers(temArray, animated: animated, completion: completion)
    }
    
    /// 移除所有当前控制器，并把viewController移到顶层
    static func repushOnly(_ viewController: UIViewController, animated: Bool = true, completion: (() -> Void)? = nil) {
        guard let navigationController = UIViewController.jw.topMost?.navigationController else { return }
        var temArray: [UIViewController] = navigationController.viewControllers.filter { $0.classForCoder != viewController.classForCoder }
        temArray.append(viewController)
        setViewControllers(temArray, animated: animated, completion: completion)
    }
}

/// 导航条Push/Pop
fileprivate extension UINavigationController {
    
    /// 带completion的Push方法
    func jwPushViewController(_ viewController: UIViewController, animated: Bool = true, completion: (() -> Void)? = nil) {
        
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        self.pushViewController(viewController, animated: animated)
        CATransaction.commit()
        
    }
    
    /// 带completion的Pop方法
    @discardableResult
    func jwPopViewController(animated: Bool = true, completion: (() -> Void)? = nil) -> UIViewController? {
        
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        let poped = self.popViewController(animated: animated)
        CATransaction.commit()
        
        return poped
        
    }
    
    /// 带completion的Pop方法
    @discardableResult
    func jwPopToViewController(_ viewController: UIViewController, animated: Bool = true, completion: (() -> Void)? = nil) -> [UIViewController]? {
        
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        let poped = self.popToViewController(viewController, animated: animated)
        CATransaction.commit()
        
        return poped
        
    }
    
    /// 带completion的Pop方法
    @discardableResult
    func jwPopToRootViewController(animated: Bool = true, completion: (() -> Void)? = nil) -> [UIViewController]? {
        
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        let poped = self.popToRootViewController(animated: animated)
        CATransaction.commit()
        
        return poped
        
    }
    
    
}
