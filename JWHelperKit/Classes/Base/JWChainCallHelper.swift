//
// @project：JWHelperKit
// @file：JWChainCall.swift
// @author：linjw(10126121@qq.com)
// @time: 2022/12/20
//  Copyright © 2018年 Linjw. All rights reserved.
//

import UIKit

/// 链式调用拓展
public extension JWNamespaceWrapper where T: AnyObject {
    
    /// 配置
    @discardableResult func config(_ config: (T) -> Void) -> T {
        config(jwWrappedValue)
        return jwWrappedValue
    }
    
    /// 配置
    @discardableResult func this(_ config: (T) -> Void) -> T {
        config(jwWrappedValue)
        return jwWrappedValue
    }

    /// 配置
    @discardableResult func then(_ config: (T) -> Void) -> T {
        config(jwWrappedValue)
        return jwWrappedValue
    }
    
}

