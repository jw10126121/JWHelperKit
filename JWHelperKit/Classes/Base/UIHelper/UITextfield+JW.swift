//
//  UITextfield+JW.swift
// @project：JWHelperKit
// @author：linjw(10126121@qq.com)
// @time: 2022/12/20
// Copyright © 2018年 Linjw. All rights reserved.
//

import UIKit

public extension JWNamespaceWrapper where T == UITextField {
    
    /// 添加左边Icon
    func addPaddingLeftIcon(_ image: UIImage?, padding: CGFloat, mode: UITextField.ViewMode = .always) {
        
        let toFrame = CGRect(x: 0, y: 0, width: (image?.size.width ?? 0) + padding, height: image?.size.height ?? 1)
        
        if jwWrappedValue.leftView == nil {
            
            let containerView = UIView(frame: toFrame)
            
            let imageView = UIImageView(frame: containerView.bounds)
            imageView.image = image
            imageView.backgroundColor = .clear
            imageView.contentMode = .center
            
            containerView.addSubview(imageView)
            
            jwWrappedValue.leftView = containerView
            jwWrappedValue.leftViewMode = mode
        }
        
        jwWrappedValue.leftView?.frame = toFrame

    }

    /// 添加右边Icon
    func addPaddingRightIcon(_ image: UIImage?, padding: CGFloat, mode: UITextField.ViewMode = .always) {
        
        let toFrame = CGRect(x: 0, y: 0, width: (image?.size.width ?? 0) + padding, height: image?.size.height ?? 1)
        
        if jwWrappedValue.rightView == nil {
            
            let containerView = UIView(frame: toFrame)
            
            let imageView = UIImageView(frame: containerView.bounds)
            imageView.image = image
            imageView.backgroundColor = .clear
            imageView.contentMode = .center
            
            containerView.addSubview(imageView)
            jwWrappedValue.clearButtonMode = .never
            jwWrappedValue.rightView = containerView
            jwWrappedValue.rightViewMode = mode
        }
        
        jwWrappedValue.rightView?.frame = toFrame
        
    }
    
    /// 添加右边自定义view
    func addPaddingRightView(_ view: UIView, padding: CGFloat, mode: UITextField.ViewMode = .always) {
        
        let toFrame = CGRect(x: 0, y: 0, width: view.frame.size.width + padding, height: view.frame.size.height)
        
        if jwWrappedValue.rightView == nil {
            
            let containerView = UIView(frame: toFrame)
            
            containerView.addSubview(view)
            jwWrappedValue.clearButtonMode = .never
            jwWrappedValue.rightView = containerView
            jwWrappedValue.rightViewMode = mode
        }
        
        jwWrappedValue.rightView?.frame = toFrame
    }
    
    
}
