//
//  UITextfield+JW.swift
//  AiXiaoYaApp
//
//  Created by linjw on 2018/8/23.
//  Copyright © 2018年 Linjw. All rights reserved.
//

import UIKit

public extension JWNamespaceWrapper where T == UITextField {
    
    /// 添加左边Icon
    func addPaddingLeftIcon(_ image: UIImage?, padding: CGFloat, mode: UITextField.ViewMode = .always) {
        
        let toFrame = CGRect(x: 0, y: 0, width: (image?.size.width ?? 0) + padding, height: image?.size.height ?? 1)
        
        if jwWrappedValue.leftView == nil {
            
            let view = UIView(frame: toFrame)
            
            let imageView = UIImageView(frame: view.bounds)
            imageView.image = image
            imageView.backgroundColor = .clear
            imageView.contentMode = .center
            
            view.addSubview(imageView)
            
            jwWrappedValue.leftView = view
            jwWrappedValue.leftViewMode = mode
        }
        
        jwWrappedValue.leftView?.frame = toFrame

    }
    
    /// 添加右边Icon
    func addPaddingRightIcon(_ image: UIImage?, padding: CGFloat, mode: UITextField.ViewMode = .always) {
        
        let toFrame = CGRect(x: 0, y: 0, width: (image?.size.width ?? 0) + padding, height: image?.size.height ?? 1)
        
        if jwWrappedValue.rightView == nil {
            
            let view = UIView(frame: toFrame)
            
            let imageView = UIImageView(frame: view.bounds)
            imageView.image = image
            imageView.backgroundColor = .clear
            imageView.contentMode = .center
            
            view.addSubview(imageView)
            jwWrappedValue.clearButtonMode = .never
            jwWrappedValue.rightView = view
            jwWrappedValue.rightViewMode = mode
        }
        
        jwWrappedValue.rightView?.frame = toFrame
        
    }
    
}
