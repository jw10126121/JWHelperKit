//
//  UIPanGestureRecognizer+JW.swift
// @project：JWHelperKit
// @author：linjw(10126121@qq.com)
// @time: 2022/12/20
// Copyright © 2018年 Linjw. All rights reserved.
//

import UIKit

/// 滑动方向
public enum PanDirection: Int {
    
    case up, down, left, right
    
    public var isVertical: Bool { return [.up, .down].contains(self) }
    
    public var isHorizontal: Bool { return !isVertical }
    
}

/// UIPanGestureRecognizer拓展
public extension JWNamespaceWrapper where T: UIPanGestureRecognizer {
    
    /// 手势方向
    var direction: PanDirection? { return jwWrappedValue.direction }
    
}

private extension UIPanGestureRecognizer {
    
    var direction: PanDirection? {
        let velocity = self.velocity(in: view)
        let isVertical = abs(velocity.y) > abs(velocity.x)
        switch (isVertical, velocity.x, velocity.y) {
        case (true, _, let y) where y < 0: return .up
        case (true, _, let y) where y > 0: return .down
        case (false, let x, _) where x > 0: return .right
        case (false, let x, _) where x < 0: return .left
        default: return nil
        }
    }
    
}
