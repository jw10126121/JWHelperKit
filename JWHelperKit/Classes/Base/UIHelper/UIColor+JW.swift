//
//  UIColor.swift
// @project：JWHelperKit
// @author：linjw(10126121@qq.com)
// @time: 2022/12/20
// Copyright © 2018年 Linjw. All rights reserved.
//

import UIKit

protocol UIColorSource {
    
    var rgbValue: UInt32 { get }
    
}

extension UInt32: UIColorSource {
    
    var rgbValue: UInt32 { return self }
}

public extension JWNamespaceWrapper where T: UIColor {
    
    /// 生成UIColor，例：UIColor.jw.color(240,240,240,1.0)
    static func color(_ r: Int, _ g: Int, _ b: Int, _ alpha: CGFloat = 1.0) -> UIColor {
        return UIColor(r: r, g: g, b: b, alpha: CGFloat(alpha))
    }
    
    /// 生成UIColor 例: UIColor(rgb: 0x000000)
    static func color(rgb rbgValue: UInt32, alpha: CGFloat = 1.0) -> UIColor {
        return UIColor(rgb: rbgValue, alpha: alpha)
    }
    
    /// 生成UIColor
    static func color(r: Int, g: Int, b: Int, alpha: CGFloat = 1.0) -> UIColor {
        return UIColor(r: r, g: g, b: b, alpha: alpha)
    }
    
    /// 生成UIColor 例: UIColor(hexString: "000000")
    static func color(hexString: String, alpha: CGFloat = 1.0) -> UIColor {
        return UIColor(hexString: hexString, alpha: alpha)
    }
    
    /// 分割线颜色 224,224,224
    static var separatorColor: UIColor { return UIColor(rgb: 0xDEDFE0, alpha: 1) }
    
    /// 视图背景色 245, 247, 248
    static var viewBackgroundColor: UIColor { return UIColor(rgb: 0xF5F7F8, alpha: 1) }
    
    /// 白色线
    static var lineWhiteColor: UIColor { return  UIColor(rgb: 0xDEDFE0, alpha: 1) }
    
    /// 灰色线
    static var lineGrayColor: UIColor { return UIColor(rgb: 0xD9D9D9, alpha: 1) }
    
}

extension UIColor {
    
    /// 生成UIColor 例: UIColor(rgb: 0x000000)
    fileprivate convenience init(rgb rgbValue: UInt32, alpha: CGFloat = 1.0) {
        self.init(red: CGFloat(Float((rgbValue & 0xFF0000) >> 16) / 255.0),
                  green: CGFloat(Float((rgbValue & 0xFF00) >> 8) / 255.0),
                  blue: CGFloat(Float(rgbValue & 0xFF) / 255.0),
                  alpha: CGFloat(alpha))
    }
    
    /// 生成UIColor
    fileprivate convenience init(r: Int, g: Int, b: Int, alpha: CGFloat = 1.0) {
        self.init(red: CGFloat(Float(r) / 255.0),
                  green: CGFloat(Float(g) / 255.0),
                  blue: CGFloat(Float(b) / 255.0),
                  alpha: CGFloat(alpha))
    }
    
    /// 生成UIColor
    fileprivate convenience init(hexString: String, alpha: CGFloat = 1.0) {

        var toString = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        
        if toString.hasPrefix("#") {
            toString = toString.jw.string(from: 1)
        }
        
        if toString.hasPrefix("0X") {
            toString = toString.jw.string(from: 2)
        }
        
        let scanner = Scanner(string: toString)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff,
            alpha: CGFloat(alpha))
    }
    
}
