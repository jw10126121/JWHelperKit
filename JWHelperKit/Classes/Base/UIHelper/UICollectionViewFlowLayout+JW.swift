//
//  UICollectionViewFlowLayout+JW.swift
// @project：JWHelperKit
// @author：linjw(10126121@qq.com)
// @time: 2022/12/20
// Copyright © 2018年 Linjw. All rights reserved.
//

import UIKit

// MARK: - UITableView拓展
public extension JWNamespaceWrapper where T: UICollectionViewFlowLayout {
    
    /// 默认配置
    func defaultStyles() {
        jwWrappedValue.minimumInteritemSpacing = 0
        jwWrappedValue.minimumLineSpacing = 0
    }
    
}
