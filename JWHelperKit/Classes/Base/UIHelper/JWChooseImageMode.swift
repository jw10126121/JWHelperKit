//
//  JWChooseImageMode.swift
// @project：JWHelperKit
// @author：linjw(10126121@qq.com)
// @time: 2022/12/20
//  Copyright © 2018年 Linjw. All rights reserved.
//

import Foundation
import UIKit


/// 选择后的图片类型
public enum JWChooseImageMode {
    
    /// app图片(本地图片，用于不需要上传到服务器的图片，而JWImageSource.localImage是需要上传到服务器，但是还没上传的)
    case appImage(UIImage?)
    /// 图片(本地图片或网络图片)
    case image(JWImageSource)
    
}


extension JWChooseImageMode: Equatable {}
