//
//  AVAudioSession+JW.swift
// @project：JWHelperKit
// @author：linjw(10126121@qq.com)
// @time: 2022/12/20
//  Copyright © 2018年 Linjw. All rights reserved.
//

import Foundation
import AVFoundation

public extension JWNamespaceWrapper where T: AVAudioSession {
    
    /**
     设置音视频后台播放
     // 后台播放音频设置,需要在Capabilities->Background Modes中勾选Audio,Airplay,and Picture in Picture
     */
    static func configCategory(_ category: AVAudioSession.Category) {
        
        do {
            let audioSession = AVAudioSession.sharedInstance()
            if #available(iOS 10.0, *) {
                try audioSession.setCategory(category, mode: AVAudioSession.Mode.default)
            } else {
                audioSession.perform(NSSelectorFromString("setCategory:error:"), with: category)
            }
            try? audioSession.setActive(true)
            
        } catch let error {
            debugPrint("audioSession error:" + error.localizedDescription)
        }
        
    }
    
}
