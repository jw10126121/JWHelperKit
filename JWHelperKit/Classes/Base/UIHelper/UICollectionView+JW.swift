//
//  UICollectionView+JW.swift
// @project：JWHelperKit
// @author：linjw(10126121@qq.com)
// @time: 2022/12/20
// Copyright © 2018年 Linjw. All rights reserved.
//

import UIKit

// MARK: - 拓展UICollectionReusableView
public extension JWNamespaceWrapper where T: UICollectionReusableView {
    
    /// 获取UICollectionReusableView所在的UICollectionView
    func collectionView() -> UICollectionView? {
        for view in sequence(first: jwWrappedValue.superview, next: { $0?.superview }) {
            if let tableView = view as? UICollectionView {
                return tableView
            }
        }
        return nil
    }
    
}
