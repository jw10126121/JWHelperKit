//
//  UISwitch+JW.swift
// @project：JWHelperKit
// @author：linjw(10126121@qq.com)
// @time: 2022/12/20
// Copyright © 2018年 Linjw. All rights reserved.
//

import UIKit

/// 拓展 UISwitch
public extension JWNamespaceWrapper where T: UISwitch {
    
    /// 值反转
    func reversed(animated: Bool = true) { jwWrappedValue.setOn(!jwWrappedValue.isOn, animated: animated) }
    
}
