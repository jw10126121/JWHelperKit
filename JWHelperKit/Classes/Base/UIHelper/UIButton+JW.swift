//
//  UIButton+JW.swift
// @project：JWHelperKit
// @author：linjw(10126121@qq.com)
// @time: 2022/12/20
// Copyright © 2018年 Linjw. All rights reserved.
//

import Foundation
import UIKit

/// 按钮内容样式
public enum JWButtonContentStyle: Int {
    
    /// [默认]左图右字，居中显示
    case leftImageRightTitle = 1
    /// 左字右图，居中显示
    case leftTitleRightImage = 2
    /// 上图下字，居中显示
    case topImageBottomTitle = 3
    /// 上字下图，居中显示
    case topTitleBottomImage = 4
    
    @available(iOS 13.0, *)
    var direction: NSDirectionalRectEdge {
        switch self {
        case .leftImageRightTitle:
            return .leading
        case .leftTitleRightImage:
            return .trailing
        case .topImageBottomTitle:
            return .top
        case .topTitleBottomImage:
            return .bottom
        }
    }
}


public extension UIButton {
    
    private struct UIButtonRTKeys {
        static var tap_touUpInside = "com.jw.app.tap_touUpInside"
    }
    
    /// 点击事件回掉
    private var actionResult: (() -> ())? {
        get {
            let view = objc_getAssociatedObject(self, &UIButtonRTKeys.tap_touUpInside) as? (() -> ())
            return view
        }
        set {
            objc_setAssociatedObject(self, &UIButtonRTKeys.tap_touUpInside, newValue as (() -> ())?, objc_AssociationPolicy.OBJC_ASSOCIATION_COPY_NONATOMIC)
        }
    }
    
    func tap(action: @escaping (() -> ())) {
        self.actionResult = action
        self.addTarget(self, action: #selector(tapAction(_:)), for: .touchUpInside)
    }
    
    @objc private func tapAction(_ sender: UIButton) {
        actionResult?()
    }
    
}

public extension UIButton {
    
    @IBInspectable
    var jwFont: UIFont? {
        get {
            return titleLabel?.font
        }
        set {
            titleLabel?.font = newValue
        }
    }
    
    @IBInspectable
    var imageForDisabled: UIImage? {
        get {
            return image(for: .disabled)
        }
        set {
            setImage(newValue, for: .disabled)
        }
    }
    
    @IBInspectable
    var imageForHighlighted: UIImage? {
        get {
            return image(for: .highlighted)
        }
        set {
            setImage(newValue, for: .highlighted)
        }
    }
    
    @IBInspectable
    var imageForNormal: UIImage? {
        get {
            return image(for: .normal)
        }
        set {
            setImage(newValue, for: .normal)
        }
    }
    
    @IBInspectable
    var imageForSelected: UIImage? {
        get {
            return image(for: .selected)
        }
        set {
            setImage(newValue, for: .selected)
        }
    }
    
    @IBInspectable
    var titleColorForDisabled: UIColor? {
        get {
            return titleColor(for: .disabled)
        }
        set {
            setTitleColor(newValue, for: .disabled)
        }
    }
    
    @IBInspectable
    var titleColorForHighlighted: UIColor? {
        get {
            return titleColor(for: .highlighted)
        }
        set {
            setTitleColor(newValue, for: .highlighted)
        }
    }
    
    @IBInspectable
    var titleColorForNormal: UIColor? {
        get {
            return titleColor(for: .normal)
        }
        set {
            setTitleColor(newValue, for: .normal)
        }
    }
    
    @IBInspectable
    var titleColorForSelected: UIColor? {
        get {
            return titleColor(for: .selected)
        }
        set {
            setTitleColor(newValue, for: .selected)
        }
    }
    
    @IBInspectable
    var titleForDisabled: String? {
        get {
            return title(for: .disabled)
        }
        set {
            setTitle(newValue, for: .disabled)
        }
    }
    
    @IBInspectable
    var titleForHighlighted: String? {
        get {
            return title(for: .highlighted)
        }
        set {
            setTitle(newValue, for: .highlighted)
        }
    }
    
    @IBInspectable
    var titleForNormal: String? {
        get {
            return title(for: .normal)
        }
        set {
            setTitle(newValue, for: .normal)
        }
    }
    
    @IBInspectable
    var titleForSelected: String? {
        get {
            return title(for: .selected)
        }
        set {
            setTitle(newValue, for: .selected)
        }
    }
    
    @IBInspectable
    var backgroundImageNormal: UIImage? {
        get {
            return backgroundImage(for: .normal)
        }
        set {
            setBackgroundImage(newValue, for: .normal)
        }
    }
    
    @IBInspectable
    var backgroundImageSelected: UIImage? {
        get {
            return backgroundImage(for: .selected)
        }
        set {
            setBackgroundImage(newValue, for: .selected)
        }
    }
    
    @IBInspectable
    var backgroundImageDisabled: UIImage? {
        get {
            return backgroundImage(for: .disabled)
        }
        set {
            setBackgroundImage(newValue, for: .disabled)
        }
    }
    
}


fileprivate class UIButtonTapActionClass: NSObject {
    
    var resultAction: (() -> ())
    
    init(action: @escaping (() -> ())) { self.resultAction = action }
    
    @objc func tapAction(_ sender: UIButton) {
        self.resultAction()
    }
    
}

// MARK: - 按钮扩展风格
public extension JWNamespaceWrapper where T: UIButton {
    
    /// 配置按钮内容样式
    func configContent(style: JWButtonContentStyle, space: CGFloat) {
        
//        if #available(iOS 15.0, *) {
//            var config = jwWrappedValue.configuration ?? UIButton.Configuration.plain()
//            config.imagePlacement = style.direction
//            config.imagePadding = space
//            jwWrappedValue.configuration = config
//            jwWrappedValue.setNeedsUpdateConfiguration()
//        } else {
            resetEdgeInsets()
            
            switch style {
            case .leftImageRightTitle:
                jwWrappedValue.titleEdgeInsets = UIEdgeInsets(top: 0, left: CGFloat(space), bottom: 0, right: -CGFloat(space))
                jwWrappedValue.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: CGFloat(space))
                break
            case .leftTitleRightImage:
                jwWrappedValue.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: CGFloat(space))
                jwWrappedValue.setNeedsLayout()
                jwWrappedValue.layoutIfNeeded()
                let contentRect = jwWrappedValue.contentRect(forBounds: jwWrappedValue.bounds)
                let titleSize = jwWrappedValue.titleRect(forContentRect: contentRect).size
                let imageSize = jwWrappedValue.imageRect(forContentRect: contentRect).size
                jwWrappedValue.titleEdgeInsets = UIEdgeInsets(top: 0, left: -imageSize.width, bottom: 0, right: imageSize.width)
                jwWrappedValue.imageEdgeInsets = UIEdgeInsets(top: 0, left: titleSize.width + CGFloat(space), bottom: 0, right: -titleSize.width - CGFloat(space))
                break
            case .topImageBottomTitle:
                jwWrappedValue.setNeedsLayout()
                jwWrappedValue.layoutIfNeeded()
                
                let contentRect = jwWrappedValue.contentRect(forBounds: jwWrappedValue.bounds)
                let titleSize = jwWrappedValue.titleRect(forContentRect: contentRect).size
                let imageSize = jwWrappedValue.imageRect(forContentRect: contentRect).size
                
                let halfWidth = (titleSize.width + imageSize.width) / 2
                let halfHeight = (titleSize.height + imageSize.height) / 2
                
                let topInset = min(halfHeight, titleSize.height)
                let leftInset = (titleSize.width - imageSize.width) > 0 ? (titleSize.width - imageSize.width) / 2 : 0
                let bottomInset = (titleSize.height - imageSize.height) > 0 ? (titleSize.height - imageSize.height) / 2 : 0
                let rightInset = min(halfWidth, titleSize.width)
                
                jwWrappedValue.titleEdgeInsets = UIEdgeInsets(top: halfHeight + CGFloat(space), left: -halfWidth, bottom: -halfHeight - CGFloat(space), right: halfWidth)
                jwWrappedValue.contentEdgeInsets = UIEdgeInsets(top: -bottomInset, left: leftInset, bottom: topInset + CGFloat(space), right: -rightInset)
                break
            case .topTitleBottomImage:
                jwWrappedValue.setNeedsLayout()
                jwWrappedValue.layoutIfNeeded()
                
                let contentRect = jwWrappedValue.contentRect(forBounds: jwWrappedValue.bounds)
                let titleSize = jwWrappedValue.titleRect(forContentRect: contentRect).size
                let imageSize = jwWrappedValue.imageRect(forContentRect: contentRect).size
                
                let halfWidth = (titleSize.width + imageSize.width) / 2
                let halfHeight = (titleSize.height + imageSize.height) / 2
                
                let topInset = min(halfHeight, titleSize.height)
                let leftInset = (titleSize.width - imageSize.width) > 0 ? (titleSize.width - imageSize.width) / 2 : 0
                let bottomInset = (titleSize.height - imageSize.height) > 0 ? (titleSize.height - imageSize.height) / 2 : 0
                let rightInset = min(halfWidth, titleSize.width)
                
                jwWrappedValue.titleEdgeInsets = UIEdgeInsets(top: -halfHeight - CGFloat(space), left: -halfWidth, bottom: halfHeight + CGFloat(space), right: halfWidth)
                jwWrappedValue.contentEdgeInsets = UIEdgeInsets(top: topInset + CGFloat(space), left: leftInset, bottom: -bottomInset, right: -rightInset)
                
                break
            }
//        }
                
    }
    
    func resetEdgeInsets() {
        jwWrappedValue.contentEdgeInsets = UIEdgeInsets.zero
        jwWrappedValue.imageEdgeInsets = UIEdgeInsets.zero
        jwWrappedValue.titleEdgeInsets = UIEdgeInsets.zero
    }
    
}

