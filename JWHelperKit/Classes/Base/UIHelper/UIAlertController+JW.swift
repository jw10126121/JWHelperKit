//
//  UIAlertController+JW.swift
// @project：JWHelperKit
// @author：linjw(10126121@qq.com)
// @time: 2022/12/20
// Copyright © 2018年 Linjw. All rights reserved.
//

import UIKit

public extension JWNamespaceWrapper where T: UIAlertController {

    var messageLabel: UILabel? {
        
        return self.jwWrappedValue.view
            .subviews[0]
            .subviews[0]
            .subviews[0]
            .subviews[0]
            .subviews[0]
            .subviews[1] as? UILabel
        
    }
    
}
