//
//  UIStackView+JW.swift
// @project：JWHelperKit
// @author：linjw(10126121@qq.com)
// @time: 2022/12/20
// Copyright © 2018年 Linjw. All rights reserved.
//

import UIKit

extension UIStackView: JWNamespaceWrappable {}

/// 拓展 UIStackView
public extension JWNamespaceWrapper where T: UIStackView {
    
    @discardableResult func defaultStyles() -> T {
        jwWrappedValue.distribution = .fill
        jwWrappedValue.alignment = .fill
        jwWrappedValue.spacing = 0
        return jwWrappedValue
    }
    
    /// 添加视图
    func addArrangedSubviews(_ views: UIView...) {
        for view in views { jwWrappedValue.addArrangedSubview(view) }
    }
    
    /// 移除视图
    func removeArrangedSubviews(_ views: UIView...) {
        for view in views {
            if view.superview == jwWrappedValue {
                jwWrappedValue.removeArrangedSubview(view)
            }
        }
    }
    
}
