//
//  JWChooseImage.swift
// @project：JWHelperKit
// @author：linjw(10126121@qq.com)
// @time: 2022/12/20
// Copyright © 2018年 Linjw. All rights reserved.
//

import UIKit

/// 上传图片源（本地图片、网络图片）
public enum JWImageSource {
    
    /// 本地图片(app固有图标)
    case appImage(UIImage?)
    /// 本地图片(选择后的图片，可能是相册或者相机图片，并非app固有图标)
    case localImage(UIImage?)
    /// 网络图片
    case networkImage(String?)
    
    /// 图片数据
    public var imageData: JWImageSourceDataProtocol? {
        switch self {
        case let .appImage(image):     return image
        case let .localImage(image):     return image
        case let .networkImage(urlText):  return urlText
        }
    }
    
    public var isNetworkImage: Bool {
        if case .networkImage(_) = self { return true }
        return false
    }
    
    public var isLocalImage: Bool {
        if case .localImage(_) = self { return true }
        if case .appImage(_) = self { return true }
        return false
    }
    
    public var imageUrl: String? {
        if case let .networkImage(value) = self { return value }
        return nil
    }
    
    public var image: UIImage? {
        if case let .localImage(value) = self { return value }
        return nil
    }
    
}

extension JWImageSource: Equatable { }

extension JWImageSource: CustomStringConvertible, CustomDebugStringConvertible {
    public var debugDescription: String {
        return self.description
    }
    
    public var description: String {
        switch self {
        case .localImage: return "本地图片"
        case .appImage: return "App图片"
        case let .networkImage(image): return "网络图片: \(image ?? "nil")"
        }
    }
    
}

public protocol JWImageSourceDataProtocol {
    var isLocalImage: Bool { get }
    var isNetworkImage: Bool { get }
}

extension String: JWImageSourceDataProtocol {
    public var isNetworkImage: Bool { return true }
    public var isLocalImage: Bool { return false }
}

extension UIImage: JWImageSourceDataProtocol {
    public var isNetworkImage: Bool { return false }
    public var isLocalImage: Bool { return true }
}

