//
//  UIStateView.swift
// @project：JWHelperKit
// @author：linjw(10126121@qq.com)
// @time: 2022/12/20
// Copyright © 2018年 Linjw. All rights reserved.
//

import UIKit

public typealias UIState = UIStateView

// Toast位置: 上、中、下
public enum UIStateToastPosition: Int {
    case top
    case center
    case bottom
}

/// UI状态指示器
public enum UIStateView {
    
    /// 默认状态，一般表示什么都不做
    case none                                                           // 什么都不做
    /// 隐藏所有指示器
    case empty(_ message: String?)                                      // 隐藏所有指示器
    /// 加载中
    case loading(_ message: String?)                                    // 加载中
    /// 加载成功
    case success(_ message: String?)                                     // 成功
    /// 加载失败
    case fail(_ message: String?)                                        // 失败
    /// toast提示在window上
    case toast(_ message: String, position: UIStateToastPosition)       // window上的toast
    /// toast提示在当前view上
    case toastInView(_ message: String, position: UIStateToastPosition)  // view上的toast
}

// MARK: - Equatable
extension UIState: Equatable {
    
    public static func ==(lhs: UIStateView, rhs: UIStateView) -> Bool {
        
        switch (lhs, rhs) {
        case (.none, .none):
            return true
        case (.empty(nil), .empty(nil)):
            return true
        case let (.loading(lhsMessage), .loading(rhsMessage)):
            return lhsMessage == rhsMessage
        case let (.success(lhsMessage), .success(rhsMessage)):
            return lhsMessage == rhsMessage
        case let (.fail(lhsMessage), .fail(rhsMessage)):
            return lhsMessage == rhsMessage
        case let (.toastInView(lhsMessage, _), .toastInView(rhsMessage, _)):
            return lhsMessage == rhsMessage
        case let (.toast(lhsMessage, _), .toast(rhsMessage, _)):
            return lhsMessage == rhsMessage
        default:
            return false
        }
    }
}
