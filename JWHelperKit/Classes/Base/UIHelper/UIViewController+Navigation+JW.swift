//
//  UIViewController+Navigation+JW.swift
// @project：JWHelperKit
// @author：linjw(10126121@qq.com)
// @time: 2022/12/20
// Copyright © 2018年 Linjw. All rights reserved.
//

import Foundation
import UIKit

public extension JWNamespaceWrapper where T: UIViewController {
    
    /// 获取最顶层活动的ViewController
    static var topMost: UIViewController? { return UIViewController.topMost }
    
    /// 获取最顶层活动的ViewController
    static func topViewController(with vc: UIViewController?) -> UIViewController? { return UIViewController.topMost(of: vc) }
    
    /// 获取设备支持方向
    static var orientation: UIInterfaceOrientationMask { return UIViewController.orientation }
    
    /// 找到当前显示的UIWindow
    fileprivate static func currentWindow() -> UIWindow? {
        
        var window: UIWindow? = UIApplication.shared.keyWindow
        if window?.windowLevel != UIWindow.Level.normal {
            for tempWindow in UIApplication.shared.windows.reversed() {
                if tempWindow.windowLevel == UIWindow.Level.normal {
                    window = tempWindow
                    break
                }
            }
        }
        return window
    }
}

@objc fileprivate extension UIViewController {
    
    /// sharedApplication
    public static var sharedApplication: UIApplication? {
        let selector = NSSelectorFromString("sharedApplication")
        return UIApplication.perform(selector)?.takeUnretainedValue() as? UIApplication
    }
    
    /// 获取设备支持的方向
    static var orientation: UIInterfaceOrientationMask {
        guard let currentWindow = UIDevice.jw.keyWindow else { return .portrait }
        return UIApplication.shared.supportedInterfaceOrientations(for: currentWindow)
    }
    
    /// 返回当前应用程序的最顶层视图控制器。
    public static var topMost: UIViewController? {
        let currentWindows = UIDevice.jw.windows
        let rootViewController = currentWindows.compactMap { $0.rootViewController }.first
        return self.topMost(of: rootViewController)
    }
        
    /// 返回给定视图控制器堆栈中最顶层的视图控制器
    ///
    /// - Parameter viewController: viewController description
    /// - Returns: return value description
    public static func topMost(of viewController: UIViewController?) -> UIViewController? {
        
        // presented view controller
        if let presentedViewController = viewController?.presentedViewController {
            return self.topMost(of: presentedViewController)
        }
        
        // UITabBarController
        if let tabBarController = viewController as? UITabBarController,
            let selectedViewController = tabBarController.selectedViewController {
            return self.topMost(of: selectedViewController)
        }
        
        // UINavigationController
        if let navigationController = viewController as? UINavigationController,
            let visibleViewController = navigationController.visibleViewController {
            return self.topMost(of: visibleViewController)
        }
        
        // UIPageController
        if let pageViewController = viewController as? UIPageViewController,
            pageViewController.viewControllers?.count == 1 {
            return self.topMost(of: pageViewController.viewControllers?.first)
        }
        
        // child view controller
        for subview in viewController?.view?.subviews ?? [] {
            if let childViewController = subview.next as? UIViewController {
                return self.topMost(of: childViewController)
            }
        }
        return viewController
    }
    
}
