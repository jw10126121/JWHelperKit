//
//  UIView+JW.swift
// @project：JWHelperKit
// @author：linjw(10126121@qq.com)
// @time: 2022/12/20
// Copyright © 2018年 Linjw. All rights reserved.
//

import UIKit

// MARK: - UIView 扩展
public extension JWNamespaceWrapper where T: UIView {
    
    /// 获取当前View所在的ViewController
    var viewController: UIViewController? {
        var next = jwWrappedValue.next
        while next != nil {
            if next?.isKind(of: UIViewController.self) == true {
                return next as? UIViewController
            }
            next = next?.next
        }
        return nil
    }
    
    /// View的safeAreaInsets
    var safeAreaInsets: UIEdgeInsets {
        guard #available(iOS 11.0, *) else { return UIEdgeInsets.zero }
        return jwWrappedValue.safeAreaInsets
    }
    
    /// 添加视图到父视图
    @discardableResult func adhere(toSuperView: UIView) -> T {
        toSuperView.addSubview(jwWrappedValue)
        return jwWrappedValue
    }
    
    /// 截图
    func screenShot() -> UIImage? {
        
        guard jwWrappedValue.frame.size.height > 0 && jwWrappedValue.frame.size.width > 0 else { return nil }
        
        UIGraphicsBeginImageContextWithOptions(jwWrappedValue.frame.size, false, 0)
        jwWrappedValue.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
    }
    
    /// 移除所有view
    func removeAllSubviews() {
        for subView in jwWrappedValue.subviews {
            subView.removeFromSuperview()
        }
    }
    
    /// 配置蒙层
    func configMaskImg(image: UIImage?, maskColor: UIColor? = nil) {
        jwWrappedValue.wConfigMaskImg(image: image, maskColor: maskColor)
    }
    
    /// 蒙层
    var circularMaskImgV: UIImageView? {
        return jwWrappedValue.circularMaskImgV
    }
    
    /// 获取大小
    func autoSize(maxWidth: CGFloat = CGFloat(UIScreen.main.bounds.size.width), maxHeight: CGFloat = 0) -> CGSize {
        return jwWrappedValue.wAutoSize(maxWidth: maxWidth, maxHeight: maxHeight)
    }
    
    /// 设置背景图
    func configBackgroundImage(_ image: UIImage?, contentMode: UIView.ContentMode = .scaleToFill) {
        jwWrappedValue.wConfigBackgroundImage(image, contentMode: contentMode)
    }
    
}

@IBDesignable public extension UIView {
    
    func wAutoSize(maxWidth: CGFloat = CGFloat(UIScreen.main.bounds.size.width), maxHeight: CGFloat = 0) -> CGSize {
        if maxWidth * maxHeight != 0 {
            return CGSize(width: CGFloat(maxWidth), height: CGFloat(maxHeight))
        }
        
        if maxHeight == 0.0 {
            let oriHeadViewSize: CGSize = self.systemLayoutSizeFitting(CGSize(width: CGFloat(maxWidth), height: CGFloat(maxHeight)),
                                                                       withHorizontalFittingPriority: UILayoutPriority.defaultHigh,
                                                                       verticalFittingPriority: UILayoutPriority.fittingSizeLevel)
            return oriHeadViewSize
        } else {
            let oriHeadViewSize: CGSize = self.systemLayoutSizeFitting(CGSize(width: CGFloat(maxWidth), height: CGFloat(maxHeight)),
                                                                       withHorizontalFittingPriority: UILayoutPriority.fittingSizeLevel,
                                                                       verticalFittingPriority: UILayoutPriority.defaultHigh)
            return oriHeadViewSize
        }
    }
    
    /// 添加圆角(要在bounds有值时，才管用)
    func wCorner(rectCorner: UIRectCorner, cornerRadius: CGSize, color: UIColor = UIColor.white) {
        
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: rectCorner, cornerRadii: cornerRadius)
        
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        mask.frame = self.bounds
        mask.fillColor = color.cgColor
        mask.strokeColor = color.cgColor
        self.layer.insertSublayer(mask, at: 0)
        
    }
    
    
    /// 圆角
    @IBInspectable dynamic var wCornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
            if self.layer.masksToBounds != true {
                self.layer.masksToBounds = true
            }
        }
    }
    
    /// 边缘宽度
    @IBInspectable dynamic var wBorderWidth: CGFloat {
        get {
            return self.layer.borderWidth
        }
        set {
            self.layer.borderWidth = newValue
            if self.layer.masksToBounds != true {
                self.layer.masksToBounds = true
            }
        }
    }
    
    /// 边缘颜色
    @IBInspectable dynamic var wBorderColor: UIColor? {
        get {
            guard let cgColor = self.layer.borderColor else {
                return nil
            }
            return UIColor(cgColor: cgColor)
        }
        set {
            self.layer.borderColor = newValue?.cgColor
            if self.layer.masksToBounds != true {
                self.layer.masksToBounds = true
            }
        }
    }
    
    /// 配置蒙层
    fileprivate func wConfigMaskImg(image: UIImage?, maskColor: UIColor? = nil) {
        
        guard let image = image else {
            if let imageView = self.circularMaskImgV, imageView.superview != nil {
                imageView.removeFromSuperview()
            }
            return
        }
        
        if self.circularMaskImgV == nil {
            self.circularMaskImgV = UIImageView(frame: self.bounds)
            self.circularMaskImgV!.backgroundColor = UIColor.clear
            self.addSubview(self.circularMaskImgV!)
            self.circularMaskImgV!.translatesAutoresizingMaskIntoConstraints = false
            if #available(iOS 9, *) {
                NSLayoutConstraint.activate([
                    self.circularMaskImgV!.leftAnchor.constraint(equalTo: leftAnchor),
                    self.circularMaskImgV!.rightAnchor.constraint(equalTo: rightAnchor),
                    self.circularMaskImgV!.topAnchor.constraint(equalTo: topAnchor),
                    self.circularMaskImgV!.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0)
                    //                self.circularMaskImgV!.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor, constant: 0)
                ])
            } else {
                let layoutAttributes: [NSLayoutConstraint.Attribute] = [.top, .leading, .bottom, .trailing]
                layoutAttributes.forEach { attribute in
                    self.addConstraint(NSLayoutConstraint(item: self.circularMaskImgV!,
                                                          attribute: attribute,
                                                          relatedBy: .equal,
                                                          toItem: self,
                                                          attribute: attribute,
                                                          multiplier: 1,
                                                          constant: 0.0))
                }
            }
        }
        
        if maskColor != nil {
            self.circularMaskImgV?.image = image.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
            self.circularMaskImgV?.tintColor = maskColor!
        } else {
            self.circularMaskImgV?.image = image
            if self.circularMaskImgV?.tintColor != nil {
                self.circularMaskImgV?.tintColor = nil
            }
        }
    }
    
}

extension UIView {

    
    func wConfigBackgroundImage(_ image: UIImage?, contentMode: UIView.ContentMode = .scaleToFill) {
        
        let backImageView = theBackgroundImageView()
        
        if backImageView.superview != nil {
            backImageView.superview?.sendSubviewToBack(backImageView)
        } else {
            self.addSubview(backImageView)
            self.sendSubviewToBack(backImageView)
            backImageView.translatesAutoresizingMaskIntoConstraints = false
            
            if #available(iOS 9, *) {
                NSLayoutConstraint.activate([
                    backImageView.leftAnchor.constraint(equalTo: leftAnchor),
                    backImageView.rightAnchor.constraint(equalTo: rightAnchor),
                    backImageView.topAnchor.constraint(equalTo: topAnchor),
                    backImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0)
                    //                backImageView.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor, constant: 0)
                ])
            } else {
                let layoutAttributes: [NSLayoutConstraint.Attribute] = [.top, .leading, .bottom, .trailing]
                layoutAttributes.forEach { attribute in
                    self.addConstraint(NSLayoutConstraint(item: backImageView,
                                                          attribute: attribute,
                                                          relatedBy: .equal,
                                                          toItem: self,
                                                          attribute: attribute,
                                                          multiplier: 1,
                                                          constant: 0.0))
                }
            }
            
        }
        backImageView.image = image
        backImageView.contentMode = contentMode
    }
    
    fileprivate func theBackgroundImageView() -> UIImageView {
        if let aView = _backgroundImageView {
            return aView
        } else {
            _backgroundImageView = UIImageView()
            _backgroundImageView?.backgroundColor = .clear
            _backgroundImageView?.contentMode = .scaleToFill
            return _backgroundImageView!
        }
    }
    
}

// 当使用setter方法时，对象本身必须是使用var声明的，即是变量
public extension JWNamespaceWrapper where T: UIView {
    
    var origin: CGPoint {
        get {
            return jwWrappedValue.frame.origin
        }
        
        set {
            var frame = jwWrappedValue.frame
            frame.origin = newValue
            jwWrappedValue.frame = frame
        }
    }
    
    var x: CGFloat {
        
        get {
            return jwWrappedValue.frame.origin.x
        }
        
        set {
            jwWrappedValue.frame.origin.x = newValue
        }
        
    }
    
    var y: CGFloat {
        
        get {
            return jwWrappedValue.frame.origin.y
        }
        
        set {
            jwWrappedValue.frame.origin.y = newValue
        }
    }
    
    var width: CGFloat {
        
        get {
            return jwWrappedValue.frame.size.width
        }
        
        set {
            jwWrappedValue.frame.size.width = newValue
        }
    }
    
    var height: CGFloat {
        
        get {
            return jwWrappedValue.frame.size.height
        }
        
        set {
            jwWrappedValue.frame.size.height = newValue
        }
        
    }
    
    var size: CGSize {
        get {
            return jwWrappedValue.frame.size
        }
        set {
            jwWrappedValue.frame.size = newValue
        }
    }
    
    var centerX: CGFloat {
        get {
            return jwWrappedValue.center.x
        }
        set {
            jwWrappedValue.center.x = newValue
        }
    }
    
    var centerY: CGFloat {
        get {
            return jwWrappedValue.center.y
        }
        set {
            var center = jwWrappedValue.center
            center.y = newValue
            jwWrappedValue.center = center
        }
    }
    
    var top: CGFloat {
        get {
            return jwWrappedValue.frame.origin.y
        }
        set {
            var frame = jwWrappedValue.frame
            frame.origin.y = newValue
            jwWrappedValue.frame = frame
        }
    }
    
    var bottom: CGFloat {
        get {
            return jwWrappedValue.frame.origin.y + jwWrappedValue.frame.size.height
        }
        set {
            var frame = jwWrappedValue.frame
            frame.origin.y = newValue - jwWrappedValue.frame.size.height
            jwWrappedValue.frame = frame
        }
    }
    
    var left: CGFloat {
        get {
            return jwWrappedValue.frame.origin.x
        }
        set {
            var frame = jwWrappedValue.frame
            frame.origin.x = newValue
            jwWrappedValue.frame = frame
        }
    }
    
    var right: CGFloat {
        get {
            return jwWrappedValue.frame.origin.x + jwWrappedValue.frame.size.width
        }
        set {
            var frame = jwWrappedValue.frame
            frame.origin.x = newValue - jwWrappedValue.frame.size.width
            jwWrappedValue.frame = frame
        }
    }
    
}

fileprivate extension UIView {
    struct UIViewRTKeys {
        static var hasCircularMaskImgV = "com.jw.app.hasCircularMaskImgV"
        static var circularMaskImgV = "com.jw.app.circularMaskImgV"
        
        static var hasBackgroundImageView = "com.jw.app.hasBackgroundImageView"
        static var backgroundImageView = "com.jw.app.backgroundImageView"
        
    }
    
    /// 蒙层
    var circularMaskImgV: UIImageView? {
        
        get {
            let view = objc_getAssociatedObject(self, &UIViewRTKeys.circularMaskImgV) as? UIImageView
            return view
        }
        set {
            objc_setAssociatedObject(self, &UIViewRTKeys.circularMaskImgV, newValue as UIImageView?, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC
            )
        }
    }
    
    /// 是否有蒙层
    var _hasCircularMaskImgV: Bool {
        get {
            return objc_getAssociatedObject(self, &UIViewRTKeys.hasCircularMaskImgV) as? Bool ?? false
        }
        set {
            objc_setAssociatedObject(self, &UIViewRTKeys.hasCircularMaskImgV, newValue as Bool?, objc_AssociationPolicy.OBJC_ASSOCIATION_ASSIGN
            )
        }
    }
    
    /// 蒙层
    var _backgroundImageView: UIImageView? {
        
        get {
            let view = objc_getAssociatedObject(self, &UIViewRTKeys.backgroundImageView) as? UIImageView
            return view
        }
        set {
            objc_setAssociatedObject(self, &UIViewRTKeys.backgroundImageView, newValue as UIImageView?, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC
            )
        }
    }
    
    /// 是否有背景图
    var _hasBackgroundImageView: Bool {
        get {
            return objc_getAssociatedObject(self, &UIViewRTKeys.hasBackgroundImageView) as? Bool ?? false
        }
        set {
            objc_setAssociatedObject(self, &UIViewRTKeys.hasBackgroundImageView, newValue as Bool?, objc_AssociationPolicy.OBJC_ASSOCIATION_ASSIGN
            )
        }
    }
    
}
