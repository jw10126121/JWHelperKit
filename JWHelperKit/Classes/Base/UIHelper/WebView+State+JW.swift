//
//  WebView+State.swift
// @project：JWHelperKit
// @author：linjw(10126121@qq.com)
// @time: 2022/12/20
// Copyright © 2018年 Linjw. All rights reserved.
//

import UIKit

/// webView加载状态
public enum WebViewLoadState: Int {
    
    /// 默认
    case none
    /// 加载中
    case loading
    /// 加载成功
    case success
    /// 加载失败
    case fail
    
}
