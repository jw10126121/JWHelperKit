//
// @file：Bool+JW.swift
// @project：JWHelperKit
// @author：linjw(10126121@qq.com)
// @time: 2022/12/20
//  Copyright © 2018年 Linjw. All rights reserved.
//

import Foundation

public extension JWNamespaceWrapper where T == Bool {

    // 转成Int类型
    var toInt: Int { return jwWrappedValue ? 1 : 0 }
    
    // 反转值
    var reversedValue: Bool { return !jwWrappedValue }
    
}
