//
//  DataBaseTableProtocol.swift
// @project：JWHelperKit
// @author：linjw(10126121@qq.com)
// @time: 2022/12/20
//  Copyright © 2018年 Linjw. All rights reserved.
//

import Foundation

/// 数据库表协议
public protocol DataBaseTableProtocol {
    
    // 数据库表名
    static func dbTableName() -> String
    
}
