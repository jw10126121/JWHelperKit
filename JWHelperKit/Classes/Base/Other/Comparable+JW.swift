//
// @file：Comparable+JW.swift
// @project：JWHelperKit
// @author：linjw(10126121@qq.com)
// @time: 2022/12/20
//  Copyright © 2018年 Linjw. All rights reserved.
//

import UIKit


public extension Comparable {
    
    private func between(minValue: Self, maxValue: Self) -> Self {
        return min(max(self, minValue), maxValue)
    }
    
    /// 限制最大、最小值
    func limit(range: ClosedRange<Self>) -> Self {
        let maxValue = range.upperBound
        let minValue = range.lowerBound
        return between(minValue: minValue, maxValue: maxValue)
    }
    
}

public extension ClosedRange {
    
    func limit(value: Bound) -> Bound {
        let maxValue = self.upperBound
        let minValue = self.lowerBound
        return Swift.min(Swift.max(value, minValue), maxValue)
    }
    
}


