//
//  NSMutableAttributedString+JW.swift
// @project：JWHelperKit
// @author：linjw(10126121@qq.com)
// @time: 2022/12/20
//  Copyright © 2018年 Linjw. All rights reserved.
//

import UIKit

public extension JWNamespaceWrapper where T == NSMutableAttributedString {
    
    @discardableResult
    func bold() -> NSMutableAttributedString {
        
        let range = (jwWrappedValue.string as NSString).range(of: jwWrappedValue.string)
        jwWrappedValue.addAttributes([NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: UIFont.systemFontSize)], range: range)
        return jwWrappedValue
    }
    
    @discardableResult
    func underline() -> NSMutableAttributedString {
        
        let range = (jwWrappedValue.string as NSString).range(of: jwWrappedValue.string)
        jwWrappedValue.addAttributes([NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue], range: range)
        return jwWrappedValue
    }
    
    @discardableResult
    func font(_ font: UIFont) -> NSMutableAttributedString {
        
        let range = (jwWrappedValue.string as NSString).range(of: jwWrappedValue.string)
        jwWrappedValue.addAttributes([.font: font], range: range)
        return jwWrappedValue
    }
    
    @discardableResult
    func foregroundColor(_ color: UIColor) -> NSMutableAttributedString {
        
        let range = (jwWrappedValue.string as NSString).range(of: jwWrappedValue.string)
        jwWrappedValue.addAttributes([.foregroundColor: color], range: range)
        return jwWrappedValue
    }
    
}
