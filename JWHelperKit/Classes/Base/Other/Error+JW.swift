//
//  Error+JW.swift
// @project：JWHelperKit
// @author：linjw(10126121@qq.com)
// @time: 2022/12/20
//  Copyright © 2018年 Linjw. All rights reserved.
//

import UIKit

/// 错误信息协议
public protocol ErrorMessageProtocol {
    /// Error信息
    var errorMessage: String { get }

}

/// 拓展错误描述信息
public extension Error {

    /// Error信息 默认实现
    var errorMessage: String {
        guard let error = self as? ErrorMessageProtocol else {
            return self.localizedDescription
        }
        return error.errorMessage
    }
    
    /// 转化为NSError
    func asNSError() -> NSError {
        return NSError(domain: self._domain, code: self._code, userInfo: [NSLocalizedDescriptionKey: self.errorMessage])
    }

}

public extension JWNamespaceWrapper where T: Error {
    
    /// Error信息 默认实现
    var errorMessage: String {
        guard let error = jwWrappedValue as? ErrorMessageProtocol else {
            return jwWrappedValue.localizedDescription
        }
        return error.errorMessage
    }
    
}
