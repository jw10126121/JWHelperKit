//
// @file：Array+JW.swift
// @project：JWHelperKit
// @author：linjw(10126121@qq.com)
// @time: 2022/12/20
//  Copyright © 2018年 Linjw. All rights reserved.
//

import Foundation

extension Array where Element: Equatable {
    
    /// 按原来的顺序，根据某个字段分组
    /// 与Dictionary(grouping: list, by: { $0.key })的区别是，后者排序不固定
    public func wGroup<K: Equatable>(by createKeyAction: ((Element) -> K)) -> [(K, [Element])] {
        var toMapList = [(K, [Element])]()
        for item in self {
            let key = createKeyAction(item)
            if toMapList.contains(where: { $0.0 == key }) {
                for (i, theItem) in toMapList.enumerated() {
                    var (aKey, theList) = theItem
                    if key == aKey, !theList.contains(where: { $0 == item }) {
                        theList.append(item)
                        toMapList[i] = (aKey, theList)
                        break
                    }
                }
            } else {
                toMapList.append((key, [item]))
            }
        }
        return toMapList
    }
    
}


extension Array {
  @inlinable
  init(reserveCapacity: Int) {
    self = Array<Element>()
    self.reserveCapacity(reserveCapacity)
  }

  @inlinable
  var slice: ArraySlice<Element> {
    self[self.startIndex ..< self.endIndex]
  }
  
  @inlinable
  subscript (safe index: Index) -> Element? {
    return indices.contains(index) ? self[index] : nil
  }
}

extension Array where Element == UInt8 {
  public init(hex: String) {
    self.init(reserveCapacity: hex.unicodeScalars.lazy.underestimatedCount)
    var buffer: UInt8?
    var skip = hex.hasPrefix("0x") ? 2 : 0
    for char in hex.unicodeScalars.lazy {
      guard skip == 0 else {
        skip -= 1
        continue
      }
      guard char.value >= 48 && char.value <= 102 else {
        removeAll()
        return
      }
      let v: UInt8
      let c: UInt8 = UInt8(char.value)
      switch c {
        case let c where c <= 57:
          v = c - 48
        case let c where c >= 65 && c <= 70:
          v = c - 55
        case let c where c >= 97:
          v = c - 87
        default:
          removeAll()
          return
      }
      if let b = buffer {
        append(b << 4 | v)
        buffer = nil
      } else {
        buffer = v
      }
    }
    if let b = buffer {
      append(b)
    }
  }

  public func toHexString() -> String {
    `lazy`.reduce(into: "") {
      var s = String($1, radix: 16)
      if s.count == 1 {
        s = "0" + s
      }
      $0 += s
    }
  }
}


