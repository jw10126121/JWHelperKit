//
//  Date+JW.swift
// @project：JWHelperKit
// @author：linjw(10126121@qq.com)
// @time: 2022/12/20
//  Copyright © 2018年 Linjw. All rights reserved.
//

import Foundation

extension Date: JWNamespaceWrappable {}

public enum JWPlayTimeTextStyle {
    /// 根据数据，自动判断用HHMMSS或MMSS
    case auto
    case MMSS
    case HHMMSS
}

/// 格式化枚举
public enum TimeFormatterType {
    case custom(String)
    case yyyyMMddHHmmss
    case yyyyMMdd
    case HHmmss // = "HH:mm:ss"
    case HHmm // = "HH:mm"
    case mmss // = "mm:ss"
    
    /// 格式化字符串
    var formatterString: String {
        switch self {
        case .HHmm:
            return "HH:mm"
        case .HHmmss:
            return "HH:mm:ss"
        case .mmss:
            return "mm:ss"
        case .yyyyMMdd:
            return "yyyy-MM-dd"
        case let .custom(formatter):
            return formatter
        case .yyyyMMddHHmmss:
            return "yyyy-MM-dd HH:mm:ss"
        }
    }
    
}

public extension JWNamespaceWrapper where T == Date {
    
    /// 转格式化文本
    func toFormattedString(_ formatterType: TimeFormatterType) -> String {
        
        let df = DateFormatter.jw.defaultDateFormatter()
        df.dateFormat = formatterType.formatterString
        
        return df.string(from: jwWrappedValue)
        
    }
    
    /// 转格式化文本
    func toFormattedString(_ formatterText: String = "yyyy-MM-dd HH:mm:ss") -> String {
        
        let df = DateFormatter.jw.defaultDateFormatter()
        df.dateFormat = formatterText
        return df.string(from: jwWrappedValue)
    }
    
}



public extension JWNamespaceWrapper where T == TimeInterval {
    
    /// 生成时间
    func playTimeText(style: JWPlayTimeTextStyle) -> String {
        let seconds = jwWrappedValue
        
        switch style {
        case .auto:
            if seconds.isNaN { return "" }
        case .MMSS:
            if seconds.isNaN { return "00:00" }
        case .HHMMSS:
            if seconds.isNaN { return "00:00:00" }
        }
        
        var minute = Int(seconds / 60)
        let second = Int(seconds.truncatingRemainder(dividingBy: 60))
        var hour = 0

        switch style {
        case .auto:
            if minute >= 60 {
                if minute >= 60 {
                    hour = Int(minute / 60)
                    minute = minute - hour * 60
                }
                return String(format: "%02d:%02d:%02d", hour, minute, second)
            } else {
                return String(format: "%02d:%02d", minute, second)
            }
        case .MMSS:
            return String(format: "%02d:%02d", minute, second)
        case .HHMMSS:
            if minute >= 60 {
                hour = Int(minute / 60)
                minute = minute - hour * 60
            }
            return String(format: "%02d:%02d:%02d", hour, minute, second)
        }
    }
    
    /// 播放时间
    func toPlayTimeStr() -> String {
        
        let secounds = jwWrappedValue
        
        if secounds.isNaN {
            return "00:00"
        }
        var minute = Int(secounds / 60)
        let second = Int(secounds.truncatingRemainder(dividingBy: 60))
        var hour = 0
        if minute >= 60 {
            hour = Int(minute / 60)
            minute = minute - hour * 60
            return String(format: "%02d:%02d:%02d", hour, minute, second)
        }
        return String(format: "%02d:%02d", minute, second)
    }
    
    /// 播放时间
    func toTimeStr() -> String {
        
        let secounds = jwWrappedValue
        
        if secounds.isNaN {
            return "00:00:00"
        }
        var minute = Int(secounds / 60)
        let second = Int(secounds.truncatingRemainder(dividingBy: 60))
        var hour = 0
        if minute >= 60 {
            hour = Int(minute / 60)
            minute = minute - hour * 60
            return String(format: "%02d:%02d:%02d", hour, minute, second)
        }
        return String(format: "%02d:%02d:%02d", hour, minute, second)
    }
    
}

extension JWNamespaceWrapper where T == String {
    
    /// 根据00:00:00时间格式，转换成秒
    public func playTimeSeconds() -> Int {
        let timeText = jwWrappedValue
        if timeText.isEmpty { return 0 }
        
        let timeArry = timeText.replacingOccurrences(of: "：", with: ":").components(separatedBy: ":")
        var seconds: Int = 0
        
        if timeArry.count > 0, let time = Int(timeArry.last ?? "0") {
            seconds += time
        }
        
        if timeArry.count > 1, let time = Int(timeArry[timeArry.count - 2] ?? "0") {
            seconds += time * 60
        }
        
        if timeArry.count > 2, let time = Int(timeArry[timeArry.count - 3] ?? "0") {
            seconds += time * 60 * 60
        }
        return seconds
    }
    
}
