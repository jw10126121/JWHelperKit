//
//  Int+JW.swift
// @project：JWHelperKit
// @author：linjw(10126121@qq.com)
// @time: 2022/12/20
//  Copyright © 2018年 Linjw. All rights reserved.
//

import Foundation
import UIKit

/// Int拓展
public extension JWNamespaceWrapper where T == Int {
    
    /// 转成Bool值
    var boolValue: Bool { return jwWrappedValue == 0 }

    /// 0..<self范围
    var range: CountableRange<Int> { return 0..<jwWrappedValue }
    
    /// 人民币字符串
    var rmb: String? { return CGFloat(jwWrappedValue).jw.rmb }
    
    /// 数字转字符串
    func digitsText(style: NumberFormatter.Style = .decimal, minDecimal: Int, maxDecimal: Int) -> String? {
        return NSNumber(value: jwWrappedValue).jw.digitsText(style: style, minDecimal: minDecimal, maxDecimal: maxDecimal)
    }
    
}
