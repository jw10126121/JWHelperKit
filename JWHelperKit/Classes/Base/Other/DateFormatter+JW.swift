//
//  DateFormatter+JW.swift
// @project：JWHelperKit
// @author：linjw(10126121@qq.com)
// @time: 2022/12/20
//  Copyright © 2018年 Linjw. All rights reserved.
//

import Foundation


public extension JWNamespaceWrapper where T: DateFormatter {

    /// 生成一个默认的DateFormatter
    static func defaultDateFormatter() -> DateFormatter {
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        df.locale = Locale(identifier: "zh_Hans_CN")
        df.calendar = Calendar(identifier: .gregorian)
        return df
    }
    
    
}
